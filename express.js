const express = require('express');
const morgan = require('morgan');
const fs = require('fs');

const app = express();
const port = 8080;

app.use(morgan('combined'));
app.use(express.json());

app.post('/api/files', (req, res) => {
    let approval = true;
    fs.readdirSync('./files').forEach((file) => {
            if (file === req.body.filename) {
                approval = false;
            }
        });
    const allowedFormatting = /.((log)|(txt)|(json)|(yaml)|(xml)|(js))$/;
    if (allowedFormatting.test(req.body.filename) & approval) {
        fs.writeFile('./files/' + req.body.filename, req.body.content, (err) => { if (err) throw err; });
        res.status(200).json({
            message: "File created succesfully"
        })
    } else {
        res.status(400).json({
            message: "Please specify 'content' parameter"
        })
    }
});

app.get('/api/files', (req, res) => {
    const filesList = [];
    fs.readdir('./files', (err, files) => {
        if (err) {
            console.log(err);
            res.sendStatus(500);
            return;
        }
        files.forEach((file) => {
            filesList.push(file);
        });
        filesList.pop(-1);
        res.status(200).json({
            message: "Success",
            files: filesList,
        })
    })
});

app.get('/api/files/:filename', (req, res) => {
    const {filename} = req.params;
    let match = false;
    const extensionRegExp = /((log)|(txt)|(json)|(yaml)|(xml)|(js))$/g;
    let uploadedDate;
    let extension = filename.match(extensionRegExp);

    fs.readdirSync('./files').forEach((file) => {
            if (file === filename) {
                match = true;
            }
        });

    fs.stat(`./files/${filename}`, (error, stats) => {
        if (error) {
            console.log(error);
            return;
            }
        uploadedDate = stats.birthtime;
    });

    if (match) {
        fs.readFile('./files/' + filename, 'utf8', function(err, data) {
            res.status(200).json({
                message: "Success",
                filename: filename,
                content: data,
                extension: extension[0],
                uploadedDate: uploadedDate
            })
        });
    } else {
        res.status(400).json({
            message: "No file with ${filename} filename found"
        })
    }
});

app.listen(port, () => {
    console.log(`Example app is listening on port ${port}`);
})